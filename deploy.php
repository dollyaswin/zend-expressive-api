<?php
namespace Deployer;

require 'recipe/zend_framework.php';

// Project name
set('application', 'Zend Expressive3 Api');

// Project repository
set('repository', 'https://bitbucket.org/dollyaswin/zend-expressive-api');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 

set('permission_method', 'chmod_777');
set('http_user', 'www-data');

// Shared files/dirs between deploys 
add('shared_files', ['config/autoload/local.php', 'data/cache/config-cache.php']);
add('shared_dirs', ['data/logs', 'data/oauth2']);

// Writable dirs by web server 
add('writable_dirs', ['data/cache', 'data/logs']);

// Hosts
host('94.237.73.55')
    ->set('branch', 'staging')
    ->stage('staging')
    ->port(22)
    ->user(getenv('STAGING_SSH_USER'))
    ->forwardAgent(true)
    ->multiplexing(true)
    ->addSshOption('UserKnownHostsFile', '/dev/null')
    ->addSshOption('StrictHostKeyChecking', 'no')
    ->set('deploy_path', getenv('STAGING_DEPLOY_PATH'));

host('94.237.73.55')
    ->set('branch', 'master')
    ->stage('production')
    ->port(22)
    ->user(getenv('PRODUCTION_SSH_USER'))
    ->forwardAgent(true)
    ->multiplexing(true)
    ->addSshOption('UserKnownHostsFile', '/dev/null')
    ->addSshOption('StrictHostKeyChecking', 'no')
    ->set('deploy_path', getenv('PRODUCTION_DEPLOY_PATH'));
    
// upload from local to server
task('upload', function () {
    upload(__DIR__ . "/", '{{release_path}}');
});

// deploy tasks
task('deploy', [
    'release',
    'cleanup',
    'success'
]);

// release tasks
task('release', [
    'deploy:prepare',
    'deploy:release',
    'upload',
    'deploy:shared',
    'deploy:writable',
    'deploy:symlink',
]);

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// database migration
// before('deploy:symlink', 'migration');
